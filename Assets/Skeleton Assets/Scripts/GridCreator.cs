﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;

namespace Skelet
{
    public class GridCreator : MonoBehaviour
    {
        [SerializeField]
        private SpriteRenderer groundPrefab;

        [SerializeField]
        private UnitDefinition[] unitDefinitions;

        [SerializeField]
        private Vector2 gridCellSize = Vector2.one;

        [SerializeField]
        private char gridFrom = 'F';

        [SerializeField]
        private char gridTo = 'T';

        [SerializeField]
        public Unit[,] TranslateStringToUnitGrid(string str, out Vector2 from, out Vector2 to)
        {
            string[] lines = str.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).Select(n => n.Trim()).Reverse().ToArray();

            int maxLength = lines.Max(n => n.Length);

            Unit[,] grid = new Unit[maxLength, lines.Length];

            from.x = (float)maxLength / 2;
            from.y = lines.Length - 1;

            to.x = (float)maxLength / 2;
            to.y = 0;

            for (int y = 0; y < lines.Length; y++)
            {
                string line = lines[y];

                for (int x = 0; x < line.Length; x++)
                {
                    var position = new Vector3(x * this.gridCellSize.x, y * this.gridCellSize.y, 0);

                    if (this.groundPrefab != null)
                    {
                        Instantiate(this.groundPrefab.gameObject, position, Quaternion.identity);
                    }

                    char c = line[x];

                    if (c == this.gridFrom)
                    {
                        from.x = x;
                        from.y = y;
                    }
                    else if (c == this.gridTo)
                    {
                        to.x = x;
                        to.y = y;
                    }

                    Unit prefab = this.GetPrefabForChar(c);
                    Unit unit = null;

                    if (prefab != null)
                    {
                        unit = (Instantiate(prefab.gameObject, position, Quaternion.identity) as GameObject).GetComponent<Unit>();
                    }

                    grid[x, y] = unit;
                }
            }

            from += this.gridCellSize * 0.5f;
            to += this.gridCellSize * 0.5f;

            return grid;
        }

        public Unit GetPrefabForChar(char c)
        {
            var definition = this.unitDefinitions.FirstOrDefault(n => n.Key == c);

            if (definition != null)
            {
                return definition.Prefab;
            }

            return null;
        }
    }

    [Serializable]
    public class UnitDefinition
    {
        [SerializeField]
        private char key;

        [SerializeField]
        private Unit prefab;

        public char Key { get { return this.key; } }

        public Unit Prefab { get { return this.prefab; } }
    }
}