using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Skelet
{
    public abstract class Enemy : MonoBehaviour
    {
        [SerializeField]
        private string key;

        [SerializeField]
        private float health = 100f;

        [SerializeField]
        private float speed = 5f;

        private int currentIndex = 0;
        private Vector2 currentTarget;
        private List<Vector2> path;

        public string Key { get { return this.key; } }

        public void SetPath(List<Vector2> path)
        {
            this.path = path;
            this.currentTarget = this.path[0];
        }

        private void Update()
        {
            this.transform.position = Vector2.MoveTowards(this.transform.position, this.currentTarget, Time.deltaTime * this.speed);

            if (Vector2.Distance(this.transform.position, this.currentTarget) <= 0.2f)
            {
                this.currentIndex++;

                if (this.currentIndex >= this.path.Count)
                {
                    GameManager.Instance.OnEnemyReachedGoal(this);
                    Destroy(this.gameObject);
                }
                else
                {
                    this.currentTarget = this.path[this.currentIndex];
                }
            }
        }
    }
}