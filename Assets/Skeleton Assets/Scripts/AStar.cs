﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This class is *not* threadsafe

public class AStar
{
    private const float SQRT2 = 1.4142135623730950488016887242097f; // Enough precision lol? This'll max it out

    private IPathNode[,] grid;
    private int gridWidth;
    private int gridHeight;
    private float gridScale = 1f;
    private bool allowDiagonal = true;

    private Coord[,] cameFromMap;
    private bool[,] closedMap;
    private bool[,] openMap;
    private float[,] gScoreMap;
    private float[,] fScoreMap;

    private int neighbourCount;
    private Coord[] neighbours = new Coord[8]; // Neighbour count can never exceed 8

    public IPathNode[,] Grid { get { return this.grid; } }

    public bool AllowDiagonal { get { return this.allowDiagonal; } set { this.allowDiagonal = value; } }

    public float GridScale
    {
        get { return this.gridScale; }
        set
        {
            if (value <= 0)
            {
                throw new ArgumentException("Grid scale cannot be set to 0 or less.");
            }

            this.gridScale = value;
        }
    }

    public Vector2 GridOffset { get; set; }

    public int GridWidth { get { return this.gridWidth; } }
    public int GridHeight { get { return this.gridHeight; } }

    public AStar(IPathNode[,] grid)
    {
        if (grid == null)
        {
            throw new ArgumentNullException("grid");
        }

        if (grid.GetLength(0) == 0 || grid.GetLength(1) == 0)
        {
            throw new ArgumentException("Grid must be at least 1 wide in the X and Y dimensions");
        }

        this.grid = grid;
        this.gridWidth = grid.GetLength(0);
        this.gridHeight = grid.GetLength(1);
    }

    public List<Vector2> GetPath(IPathNode from, IPathNode to, string key)
    {
        if (from == null)
        {
            throw new ArgumentNullException("from");
        }

        if (to == null)
        {
            throw new ArgumentNullException("to");
        }

        if (key == null)
        {
            throw new ArgumentNullException("key");
        }

        // We'll have to search the entire grid for the nodes in question, so we can figure out where they are
        int fromX = -1, fromY = -1, toX = -1, toY = -1;
        bool fromSet = false, toSet = false;

        for (int x = 0; x < this.gridWidth; x++)
        {
            for (int y = 0; y < this.gridHeight; y++)
            {
                if (this.grid[x, y] == from)
                {
                    fromX = x;
                    fromY = y;

                    fromSet = true;
                }

                if (this.grid[x, y] == to)
                {
                    toX = x;
                    toY = y;

                    toSet = true;
                }

                if (fromSet && toSet)
                {
                    break;
                }
            }
        }

        if (fromSet && toSet)
        {
            return this.GetPath(fromX, fromY, toX, toY, key);
        }

        throw new ArgumentException("The given nodes are not in the pathfinding grid");
    }

    /// <summary>
    /// Find a path given two positions in the grid. Note that the grid starts from (0,0), and then extends to (width,height) in dimensions, multiplied by the grid scale.
    /// </summary>
    public List<Vector2> GetPath(Vector2 from, Vector2 to, string key)
    {
        int fromX = Mathf.RoundToInt(Mathf.Clamp((from.x - this.GridOffset.x) / this.gridScale, 0, this.gridWidth - 1));
        int fromY = Mathf.RoundToInt(Mathf.Clamp((from.y - this.GridOffset.y) / this.gridScale, 0, this.gridHeight - 1));
        int toX = Mathf.RoundToInt(Mathf.Clamp((to.x - this.GridOffset.x) / this.gridScale, 0, this.gridWidth - 1));
        int toY = Mathf.RoundToInt(Mathf.Clamp((to.y - this.GridOffset.y) / this.gridScale, 0, this.gridHeight - 1));

        var path = this.GetPath(fromX, fromY, toX, toY, key);

        if (path != null)
        {
            // Add precise start and end points
            path.Insert(0, from);
            path.Add(to);
        }

        return path;
    }

    public List<Vector2> GetPath(int fromX, int fromY, int toX, int toY, string key)
    {
        if (fromX < 0 || fromX >= this.gridWidth
            || fromY < 0 || fromY >= this.gridHeight
            || toX < 0 || toX >= this.gridWidth
            || toY < 0 || toY >= this.gridHeight)
        {
            throw new ArgumentException(string.Format("Given coordinates from ({0}, {1}) to ({2}, {3}) are not within the grid of size ({4}, {5})", fromX, fromY, toX, toY, this.gridWidth, this.gridHeight));
        }

        var fromNode = this.grid[fromX, fromY];
        var toNode = this.grid[toX, toY];

        if ((fromNode != null && !fromNode.IsPassable(key)) || (toNode != null && !toNode.IsPassable(key)))
        {
            // Either from or to node is not passable
            return null;
        }

        if (fromX == toX && fromY == toY)
        {
            // If from and to are the same, we just return a path with one node going through there
            return new List<Vector2>() { new Vector2(fromX * this.gridScale, fromY * this.gridScale) + this.GridOffset };
        }

        this.ResetMaps();

        // This should really be a priority queue of some sort, but .NET doesn't have one of those, so this is the stopgap solution
        LinkedList<Coord> open = new LinkedList<Coord>();
        Dictionary<Coord, LinkedListNode<Coord>> coordOpenMap = new Dictionary<Coord, LinkedListNode<Coord>>(); // This is just for speedup

        var from = new Coord(fromX, fromY);
        var to = new Coord(toX, toY);

        open.AddFirst(new Coord(fromX, fromY));
        this.openMap[fromX, fromY] = true;
        this.gScoreMap[fromX, fromY] = 0f;
        this.fScoreMap[fromX, fromY] = this.Heuristic(from, to);

        while (open.Count > 0)
        {
            // Get current and remove it from open list
            Coord current = open.First.Value;
            open.RemoveFirst();
            coordOpenMap.Remove(current);
            this.openMap[current.X, current.Y] = false;

            if (current.X == toX && current.Y == toY)
            {
                // We've reached the goal
                return this.ReconstructPath(current);
            }
            // Close current node
            this.closedMap[current.X, current.Y] = true;

            // Fill neighbour array with information
            this.SetNeighbours(current, key);

            for (int i = 0; i < this.neighbourCount; i++)
            {
                var neighbour = this.neighbours[i];
                bool isNew = false;

                if (this.closedMap[neighbour.X, neighbour.Y])
                {
                    // This neighbour is already closed
                    continue;
                }

                float tentativeGScore = this.gScoreMap[current.X, current.Y] + this.NeighbourDist(current, neighbour);
                float tentativeFScore = tentativeGScore + this.Heuristic(neighbour, to);

                if (this.openMap[neighbour.X, neighbour.Y] == false)
                {
                    // This is a new node; added it to the open list and mark it as open
                    this.openMap[neighbour.X, neighbour.Y] = true;
                    this.InsertIntoOpen(open, coordOpenMap, tentativeFScore, neighbour);
                    isNew = true;
                }
                else if (tentativeGScore >= gScoreMap[neighbour.X, neighbour.Y])
                {
                    // This neighbour is open, but going to this neighbour through the current node is not a better path; no need to update maps
                    continue;
                }

                // The path through current is the best until now for this neighbour - update maps
                this.cameFromMap[neighbour.X, neighbour.Y] = current;
                this.gScoreMap[neighbour.X, neighbour.Y] = tentativeGScore;
                this.fScoreMap[neighbour.X, neighbour.Y] = tentativeFScore;

                if (isNew == false)
                {
                    // We now need to update this neighbour's position in the open linked list, since that has to be kept sorted
                    this.RemoveFromOpen(open, coordOpenMap, neighbour);
                    this.InsertIntoOpen(open, coordOpenMap, tentativeFScore, neighbour);
                }
            }
        }

        return null;
    }

    private void RemoveFromOpen(LinkedList<Coord> open, Dictionary<Coord, LinkedListNode<Coord>> coordOpenMap, Coord node)
    {
        var listNode = coordOpenMap[node];
        open.Remove(listNode);
    }

    private void InsertIntoOpen(LinkedList<Coord> open, Dictionary<Coord, LinkedListNode<Coord>> coordOpenMap, float fScore, Coord node)
    {
        // New node discovered
        var curLinkedNode = open.First;

        // Insert sorted by fScore
        while (true)
        {
            if (curLinkedNode == null)
            {
                open.AddLast(node);
                coordOpenMap[node] = open.Last;
                break;
            }

            var val = curLinkedNode.Value;

            if (this.fScoreMap[val.X, val.Y] > fScore)
            {
                open.AddBefore(curLinkedNode, node);
                coordOpenMap[node] = curLinkedNode.Previous;
                break;
            }

            curLinkedNode = curLinkedNode.Next;
        }
    }

    // Returns the lowest possible distance between the nodes from and to
    private float Heuristic(Coord from, Coord to)
    {
        var delta = new Coord(Mathf.Abs(from.X - to.X), Mathf.Abs(from.Y - to.Y));

        if (this.allowDiagonal)
        {
            if (delta.X > delta.Y)
            {
                return delta.Y * SQRT2 + (delta.X - delta.Y);
            }
            else
            {
                return delta.X * SQRT2 + (delta.Y - delta.X);
            }
        }
        else
        {
            return delta.X + delta.Y;
        }
    }

    private float NeighbourDist(Coord from, Coord to)
    {
        if (from.X == to.X || from.Y == to.Y)
        {
            return 1f;
        }
        else
        {
            return SQRT2;
        }
    }

    private List<Vector2> ReconstructPath(Coord end)
    {
        List<Vector2> result = new List<Vector2>();

        Coord current = end;

        while (current.X >= 0 && current.Y >= 0)
        {
            result.Add(new Vector2(current.X * this.gridScale, current.Y * this.gridScale) + this.GridOffset);
            current = this.cameFromMap[current.X, current.Y];
        }

        result.Reverse();
        return result;
    }

    private void SetNeighbours(Coord node, string key)
    {
        int count = 0;

        bool left = false,
             top = false,
             right = false,
             down = false;

        if (node.X > 0)
        {
            // Center left
            var coord = new Coord(node.X - 1, node.Y);

            if (this.NodeIsPassable(coord, key))
            {
                this.neighbours[count++] = coord;
                left = true;
            }
        }

        if (node.Y < this.gridHeight - 1)
        {
            // Top center
            var coord = new Coord(node.X, node.Y + 1);

            if (this.NodeIsPassable(coord, key))
            {
                this.neighbours[count++] = coord;
                top = true;
            }
        }

        if (node.X < this.gridWidth - 1)
        {
            // Center right
            var coord = new Coord(node.X + 1, node.Y);

            if (this.NodeIsPassable(coord, key))
            {
                this.neighbours[count++] = coord;
                right = true;
            }
        }

        if (node.Y > 0)
        {
            // Bottom center
            var coord = new Coord(node.X, node.Y - 1);

            if (this.NodeIsPassable(coord, key))
            {
                this.neighbours[count++] = coord;
                down = true;
            }
        }

        if (this.allowDiagonal)
        {
            if (node.X > 0)
            {
                if (down && left && node.Y > 0)
                {
                    // Bottom left
                    var coord = new Coord(node.X - 1, node.Y - 1);

                    if (this.NodeIsPassable(coord, key))
                    {
                        this.neighbours[count++] = coord;
                    }
                }

                if (top && left && node.Y < this.gridHeight - 1)
                {
                    // Top left
                    var coord = new Coord(node.X - 1, node.Y + 1);

                    if (this.NodeIsPassable(coord, key))
                    {
                        this.neighbours[count++] = coord;
                    }
                }
            }

            if (node.X < this.gridWidth - 1)
            {
                if (top && right && node.Y < this.gridHeight - 1)
                {
                    // Top right
                    var coord = new Coord(node.X + 1, node.Y + 1);

                    if (this.NodeIsPassable(coord, key))
                    {
                        this.neighbours[count++] = coord;
                    }
                }

                if (down && right && node.Y > 0)
                {
                    // Bottom right
                    var coord = new Coord(node.X + 1, node.Y - 1);

                    if (this.NodeIsPassable(coord, key))
                    {
                        this.neighbours[count++] = coord;
                    }
                }
            }
        }

        this.neighbourCount = count;
    }

    private bool NodeIsPassable(Coord node, string key)
    {
        var pathNode = this.grid[node.X, node.Y];

        return pathNode == null || pathNode.IsPassable(key);
    }

    private void ResetMaps()
    {
        if (this.cameFromMap == null || this.closedMap == null)
        {
            this.cameFromMap = new Coord[this.gridWidth, this.gridHeight];
            this.closedMap = new bool[this.gridWidth, this.gridHeight];
            this.openMap = new bool[this.gridWidth, this.gridHeight];
            this.gScoreMap = new float[this.gridWidth, this.gridHeight];
            this.fScoreMap = new float[this.gridWidth, this.gridHeight];
        }

        for (int x = 0; x < this.gridWidth; x++)
        {
            for (int y = 0; y < this.gridHeight; y++)
            {
                this.cameFromMap[x, y] = new Coord(-1, -1);
                this.closedMap[x, y] = false;
                this.openMap[x, y] = false;
                this.gScoreMap[x, y] = float.MaxValue;
                this.fScoreMap[x, y] = float.MaxValue;
            }
        }
    }

    private struct Coord
    {
        public int X;
        public int Y;

        public Coord(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
    }
}

public interface IPathNode
{
    /// <summary>
    /// Checks whether a certain key can pass this node.
    /// </summary>
    bool IsPassable(string key);
}